// const toggleTheme = document.getElementById('toggleTheme');
// toggleTheme.addEventListener('click', (e) => {});

// try {
//     if (localStorage.getItem('theme') === 'dark') {
//         document.querySelector('html').classList.add('dark');
//     }
// } catch (err) {}

// document.getElementById('toggleTheme').addEventListener('click', (e) => {
//     e.preventDefault();
//     if (localStorage.getItem('theme') === 'dark') {
//         localStorage.removeItem('theme');
//     } else {
//         localStorage.setItem('theme', 'dark');
//     }
//     if (localStorage.getItem('theme') === 'dark') {
//         let link = document.getElementById('themeStylesheet');
//         link.href = 'css/dark-theme.css';
//     }
//     addDarkClassToHTML();
// });
//
// function addDarkClassToHTML() {
//     try {
//         if (localStorage.getItem('theme') === 'dark') {
//             document.querySelector('html').classList.add('dark');
//         } else {
//             document.querySelector('html').classList.remove('dark');
//         }
//     } catch (err) {}
// }
// addDarkClassToHTML();

document.getElementById('toggleTheme').addEventListener('click', (e) => {
    e.preventDefault();
    if (localStorage.getItem('theme') === 'dark') {
        localStorage.removeItem('theme');
    } else {
        localStorage.setItem('theme', 'dark');
    }
    updateTheme();
});

function updateTheme() {
    const theme = localStorage.getItem('theme');
    const link = document.getElementById('themeStylesheet');

    if (theme === 'dark') {
        link.href = 'css/dark-theme.css';
        document.querySelector('html').classList.add('dark');
    } else {
        link.href = 'css/main.css';
        document.querySelector('html').classList.remove('dark');
    }
}

// Инициализация темы при загрузке страницы
updateTheme();
